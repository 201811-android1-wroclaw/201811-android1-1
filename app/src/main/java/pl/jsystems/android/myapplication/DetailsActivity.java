package pl.jsystems.android.myapplication;

import java.net.URI;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {
    private TextView tvInput;
    private Button btnCall, btnBrowser;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        tvInput = findViewById(R.id.tv_input);
        btnCall = findViewById(R.id.btn_call);
        btnBrowser = findViewById(R.id.btn_browser);

        if (getIntent().hasExtra("username")) {
            username = getIntent().getStringExtra("username");
            tvInput.setText(username);
            btnCall.setVisibility(isPhoneNumber(username) ? View.VISIBLE : View.GONE);
            btnBrowser.setVisibility(isUrl(username) ? View.VISIBLE : View.GONE);
            // Adres: Uri.parse
            // Numer telefonu: RegExp - 3 lub wiecej liczb
        }
    }

    private boolean isUrl(String text) {
        try {
            URI.create(text);
            return true;
        } catch (Throwable ex) {
            return false;
        }
    }

    private boolean isPhoneNumber(String text) {
        return text.matches("^[\\d]{3,10}$");
    }

    public void onCallClick(View v) {
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + username));
        startActivity(callIntent);
    }

    public void onBrowserClick(View v) {
        Uri uri = Uri.parse(username);
        if (uri.getScheme() == null || uri.getScheme().equals("")) {
            uri = Uri.fromParts("http", username, null);
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(browserIntent);
    }
}
